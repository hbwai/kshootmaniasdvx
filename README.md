# SDVX Charts for KShootMania #

SDVX Song packages meant for use in KShootMania. 

These charts follows the offical sdvx charts closely to emulate the original experience.

## Setting up the charts for playing & editing ##
1. Empty out the directory of "kshootmania\songs"
2. Clone the repository directly into "kshootmania\songs"

You may revert your directory to its original state after cloning (This repository is configured to ignore all other songs)

We configure the directory this way for easy editing and playing the charts.

# Creating SDVX Charts #

Official SDVX Charts can be found over at http://www.sdvx.be/

Find the chart that you will like to recreate on KSH

Youtube tutorial for chart editing:

https://www.youtube.com/watch?v=fRnLNiD2DRg
## 1. Create song folder ##
To create a song, you will first need to create a folder named after the song in "kshootmania/songs/sdvx"

The folder named **must satisfy** the following conditions:

* Lower case letters only
* Alphabets only (No special characters)
* No whitespace
* Japanese characters to be converted into romanji

### Example English Songname ###
I'm So Happy
### Expected folder name ###
![imsohappy](https://bitbucket.org/repo/5y4rb6/images/1595790915-5aa210b9efc04de56eb571bb5d0a35ee_1429729340.png)
### Example Japanese Songname ###
恋する☆宇宙戦争っ!!
### Romanji name ###
Koisuru ☆ uchū sensō ~tsu! !
### Expected folder name ###
![9ff6d5fe21c7f71d8782ad56df821d2b_1429729449.png](https://bitbucket.org/repo/5y4rb6/images/2016589420-9ff6d5fe21c7f71d8782ad56df821d2b_1429729449.png)
## 2. Creating a .ksh chart ##
![ChartMetaDesc.png](https://bitbucket.org/repo/5y4rb6/images/558140521-ChartMetaDesc.png)
![ChartMetadataDetails.png](https://bitbucket.org/repo/5y4rb6/images/4261009097-ChartMetadataDetails.png)
![GenreIcon.png](https://bitbucket.org/repo/5y4rb6/images/4226803361-GenreIcon.png)